module.exports = function(grunt) {
	require('jit-grunt')(grunt);
    grunt.initConfig({
        sass: {
            options: {
                sourceMap: true,
				outputStyle: 'expanded', //Values: nested, expanded, compact, compressed
				precision : 5
            },
            dist: {
                files: {
                    'css/style.css': 'scss/style.scss'
                }
            }
        },
        postcss: {
            options: {
                map: true,
                processors: [
                    require('autoprefixer')()
                ]
            },
            dist: {
                src: 'css/style.css',
                dest: 'css/style.css'
            }
        },
        csscomb: {
            dist: {
                files: {
                    'css/style.css': ['css/style.css']
                }
            }
        },
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['sass', 'postcss','csscomb']
            }
        }
    });
    grunt.registerTask('default', ['sass','postcss','csscomb','watch']);
}
