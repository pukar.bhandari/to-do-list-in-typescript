var ToDoList = /** @class */ (function () {
    function ToDoList() {
        this.pending = document.getElementById('pending-tasks');
        this.completed = document.getElementById('completed-tasks');
        this.showData();
        this.btnAddEventListen();
        this.btnDeleteEventListen();
        this.editEventListen();
    }
    ToDoList.prototype.showData = function () {
        this.pending.innerHTML = "";
        this.completed.innerHTML = "";
        this.apiRequest("GET", "http://localhost:3000/db", null);
    };
    ToDoList.prototype.btnAddEventListen = function () {
        var _this = this;
        var btn = document.getElementById('add');
        var items = document.getElementById('todo');
        btn.addEventListener('click', function () {
            _this.sendData();
        });
        items.addEventListener("keyup", function (event) {
            if (event.key === "Enter") {
                event.preventDefault();
                btn.click();
            }
        });
    };
    ToDoList.prototype.btnDeleteEventListen = function () {
        var _this = this;
        var wrap = document.getElementById('wrapper');
        wrap.addEventListener('click', function (ev) {
            var button = ev.target;
            var input = ev.target;
            if (button && button.matches('button.delete')) {
                var datId = button.attributes["data-id"].value;
                if (button.parentNode.parentNode === _this.pending) {
                    _this.apiRequest("DELETE", "http://localhost:3000/pending/" + datId, null);
                    //(<HTMLLIElement>button.parentNode).remove();
                    return;
                }
                _this.apiRequest("DELETE", "http://localhost:3000/completed/" + datId, null);
                //(<HTMLLIElement>button.parentNode).remove();
            }
            if ((input) && (input).matches('input[type = checkbox]')) {
                _this.toggleStatus(input);
            }
        });
    };
    ToDoList.prototype.editEventListen = function () {
        var _this = this;
        var tasks = document.querySelector('.tasks');
        tasks.addEventListener('dblclick', function (ev) {
            var lab = ev.target;
            if (lab && lab.matches('label')) {
                var t = lab.innerHTML;
                lab.innerHTML = "<input type=\"text\" value=\"" + t + "\">";
                lab.addEventListener('keyup', function (event) {
                    var btn1 = lab.parentNode.querySelector('button');
                    var val = btn1.attributes['data-id'].value;
                    if (event.key === "Enter") {
                        if (lab.querySelector('input').value !== null) {
                            lab.innerHTML = lab.querySelector('input').value;
                        }
                        if (lab.innerHTML === "") {
                            var guardian = lab.parentNode;
                            if (guardian.parentNode === _this.pending) {
                                _this.apiRequest("DELETE", "http://localhost:3000/pending/" + val, null);
                            }
                            _this.apiRequest("DELETE", "http://localhost:3000/completed/" + val, null);
                            //guardian.remove();
                        }
                    }
                });
            }
        });
    };
    ToDoList.prototype.toggleStatus = function (checkbox) {
        var clone = checkbox.parentElement.cloneNode(true);
        var val = checkbox.parentNode.querySelector('button');
        var datId = val.attributes["data-id"].value;
        var data = JSON.stringify({
            title: clone.querySelector('label').innerHTML
        });
        if (checkbox.checked) {
            //this.completed.appendChild(clone);
            this.apiRequest('PATCH', "http://localhost:3000/completed/" + datId, data);
            //this.apiRequest("DELETE", "http://localhost:3000/pending/" + datId, null);
        }
        else {
            //this.pending.appendChild(clone);
            this.apiRequest('PATCH', "http://localhost:3000/pending/" + datId, data);
            //this.apiRequest("DELETE", "http://localhost:3000/completed/" + datId, null);
        }
        //checkbox.parentNode.remove();
    };
    ToDoList.prototype.sendData = function () {
        this.items = document.getElementById('todo');
        if (this.items.value === "") {
            return alert("Enter something");
        }
        var item = this.items.value;
        this.items.value = "";
        var data = JSON.stringify({
            title: item
        });
        this.apiRequest("POST", "http://localhost:3000/pending", data);
    };
    ToDoList.prototype.apiRequest = function (method, url, data) {
        var that = this;
        var httpRequest = new XMLHttpRequest();
        httpRequest.open(method, url, true);
        if (method === "GET") {
            httpRequest.onload = function () {
                var toJson = JSON.parse(this.responseText);
                for (var i = 0; i < toJson.pending.length; i++) {
                    that.pending.innerHTML += "<li class=\"allTasks clearfix\">\n    <input class=\"ptasks\" type=\"checkbox\" /> \n        <label for=\"ptasks\">" + toJson.pending[i].title + "</label>\n        <button class=\"delete\" data-id=" + toJson.pending[i].id + ">&#10005;Delete</button></li>";
                }
                for (var i = 0; i < toJson.completed.length; i++) {
                    that.completed.innerHTML += "<li class=\"allTasks clearfix\">\n                         <input class=\"ctasks\" type=\"checkbox\" /> \n                         <label for=\"ctasks\">" + toJson.completed[i].title + "</label>\n                          <button class=\"delete\" data-id=" + toJson.completed[i].id + ">&#10005;Delete</button></li>";
                }
                var check = document.querySelectorAll('input[type=checkbox]');
                for (var i = 0; i < check.length; i++) {
                    if (check[i].parentNode.parentNode === that.completed) {
                        check[i].checked = true;
                    }
                }
            };
            httpRequest.send();
        }
        if (method === "POST") {
            httpRequest.onload = function () {
                that._displayData(JSON.parse(this.responseText));
            };
            httpRequest.setRequestHeader('Content-Type', 'application/json');
            httpRequest.send(data);
            this.showData();
        }
        if (method === "DELETE") {
            httpRequest.send(data);
            this.showData();
        }
        if (method === "PATCH") {
            httpRequest.setRequestHeader('Content-Type', 'application/json');
            httpRequest.send(data);
            this.showData();
        }
    };
    ToDoList.prototype._displayData = function (item) {
        this.pending.innerHTML += "<li class=\"allTasks clearfix\">\n    <input class=\"ptasks\" type=\"checkbox\" />\n        <label for=\"ptasks\">" + item['title'] + "</label>\n        <button class=\"delete\" data-id=" + item.id + ">&#10005;Delete</button></li>";
    };
    return ToDoList;
}());
///<reference path="script.ts"/>
var listItem = new ToDoList();
//# sourceMappingURL=app.js.map