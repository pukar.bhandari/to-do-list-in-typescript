class ToDoList{
    items: HTMLInputElement;
    pending: HTMLElement;
    completed: HTMLElement;

    constructor() {
        this.pending = document.getElementById('pending-tasks');
        this.completed = document.getElementById('completed-tasks');
        this.showData();
        this.btnAddEventListen();
        this.btnDeleteEventListen();
        this.editEventListen();
    }

    showData() {
        this.pending.innerHTML = "";
        this.completed.innerHTML = "";
        this.apiRequest("GET", "http://localhost:3000/db", null);
    }

     btnAddEventListen(){
         let btn = document.getElementById('add');
         let items = document.getElementById('todo');
         btn.addEventListener('click', () => {
             this.sendData();
         });
         items.addEventListener("keyup", function (event) {
             if (event.key === "Enter") {
                 event.preventDefault();
                 btn.click();
             }
         });
     }

     btnDeleteEventListen(){
         let wrap = document.getElementById('wrapper');
         wrap.addEventListener('click', (ev) => {
             let button = <HTMLButtonElement>ev.target;
             let input = <HTMLInputElement>ev.target;
             if (button && button.matches('button.delete')) {
                 let datId = button.attributes["data-id"].value;
                 if (button.parentNode.parentNode === this.pending) {
                     this.apiRequest("DELETE", "http://localhost:3000/pending/" + datId, null);
                     //(<HTMLLIElement>button.parentNode).remove();
                     return
                 }
                 this.apiRequest("DELETE", "http://localhost:3000/completed/" + datId, null);
                 //(<HTMLLIElement>button.parentNode).remove();
             }

             if ((input) && (input).matches('input[type = checkbox]')) {
                 this.toggleStatus(input);
             }
         });
    }

    editEventListen() {
        let tasks = document.querySelector('.tasks');
        tasks.addEventListener('dblclick', (ev) => {
            let lab = <HTMLLabelElement>ev.target;
            if (lab && lab.matches('label')) {
                const t = lab.innerHTML;
                lab.innerHTML = `<input type="text" value="${t}">`;
                lab.addEventListener('keyup', (event)=> {
                    let btn1 = lab.parentNode.querySelector('button');
                    let val = btn1.attributes['data-id'].value;
                    if (event.key === "Enter") {
                        if (lab.querySelector('input').value !== null) {
                            lab.innerHTML = lab.querySelector('input').value;
                        }
                        if (lab.innerHTML === "") {
                            let guardian = <HTMLLIElement>lab.parentNode;
                            if (guardian.parentNode === this.pending) {
                                this.apiRequest("DELETE", "http://localhost:3000/pending/" + val, null);
                            }
                            this.apiRequest("DELETE", "http://localhost:3000/completed/" + val, null);
                            //guardian.remove();
                        }
                    }
                })
            }
        });
    }

    toggleStatus(checkbox) {
        let clone = checkbox.parentElement.cloneNode(true);
        let val = checkbox.parentNode.querySelector('button');
        let datId = val.attributes["data-id"].value;
        let data = JSON.stringify({
            title: clone.querySelector('label').innerHTML
        });
        if (checkbox.checked) {
            //this.completed.appendChild(clone);
            this.apiRequest('PATCH', "http://localhost:3000/completed/" + datId, data);
            //this.apiRequest("DELETE", "http://localhost:3000/pending/" + datId, null);
        } else {
            //this.pending.appendChild(clone);
            this.apiRequest('PATCH', "http://localhost:3000/pending/" + datId, data);
            //this.apiRequest("DELETE", "http://localhost:3000/completed/" + datId, null);
        }
        //checkbox.parentNode.remove();
    }

    sendData() {
        this.items = <HTMLInputElement>document.getElementById('todo');
        if (this.items.value === "") {
            return alert("Enter something");
        }
        const item = this.items.value;
        this.items.value = "";
        let data = JSON.stringify({
            title: item
        });
        this.apiRequest("POST", "http://localhost:3000/pending", data);
    }

    apiRequest(method, url, data) {
        let that = this;
        let httpRequest = new XMLHttpRequest();
        httpRequest.open(method, url, true);
        if (method === "GET") {
            httpRequest.onload = function () {
                let toJson = JSON.parse(this.responseText);
                for (let i = 0; i < toJson.pending.length; i++) {
                    that.pending.innerHTML += `<li class="allTasks clearfix">
    <input class="ptasks" type="checkbox" /> 
        <label for="ptasks">${toJson.pending[i].title}</label>
        <button class="delete" data-id=${toJson.pending[i].id}>&#10005;Delete</button></li>`;
                }

                for (let i = 0; i < toJson.completed.length; i++) {
                    that.completed.innerHTML += `<li class="allTasks clearfix">
                         <input class="ctasks" type="checkbox" /> 
                         <label for="ctasks">${toJson.completed[i].title}</label>
                          <button class="delete" data-id=${toJson.completed[i].id}>&#10005;Delete</button></li>`;
                }
                let check: NodeListOf<HTMLInputElement> = document.querySelectorAll('input[type=checkbox]');
                for (let i = 0; i < check.length; i++) {
                    if (check[i].parentNode.parentNode === that.completed) {
                        check[i].checked = true;
                    }
                }
            };
            httpRequest.send();
        }

        if (method === "POST") {
            httpRequest.onload = function(){
                that._displayData(JSON.parse(this.responseText))
            };
            httpRequest.setRequestHeader('Content-Type', 'application/json')
            httpRequest.send(data);
            this.showData();
        }

        if (method === "DELETE") {
            httpRequest.send(data);
            this.showData();
        }

        if(method === "PATCH"){
            httpRequest.setRequestHeader('Content-Type', 'application/json')
            httpRequest.send(data);
            this.showData();
        }
    }

    private _displayData(item: any){
        this.pending.innerHTML += `<li class="allTasks clearfix">
    <input class="ptasks" type="checkbox" />
        <label for="ptasks">${item['title']}</label>
        <button class="delete" data-id=${item.id}>&#10005;Delete</button></li>`;
    }
}

